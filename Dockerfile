# Аргументы сборки
ARG NODE_VERSION=18.16.0

# Используем базовый образ Node.js (синоним назначим base)
FROM node:${NODE_VERSION}-slim as base

# Переменные окружения
ENV NODE_ENV=production

#----------------------------------------------------------
# Стадия сборки исполнительнимых файлов проекта для образа
FROM base as build

# Переменные окружения
ENV NODE_ENV=production

# Все приложения крутятся в каталогах /opt/ внутри итогового образа
WORKDIR /opt/app/

# Устанавливаем необходимые пакеты на основе списка пакетов проекта.
# COPY package.json . 
COPY --link package.json .
RUN yarn install --production=false
COPY --link . .
# RUN npm ci
RUN yarn build

#----------------------------------------------------------
# Стадия сборки  образа
FROM base

# Переменные окружения
ENV NODE_ENV=production
ENV PORT=3000

# Все приложения крутятся в каталогах /opt/ внутри итогового образа
WORKDIR /opt/app/

COPY --from=build /opt/app/.output /opt/app/.output

# Открываем порт
EXPOSE 3000
# Команда запуска сервиса
CMD [ "node", ".output/server/index.mjs" ]