# LMAdviser.FE

## Установка локально

```bash
# Установка пакетов
yarn install

# Запуск приложения в dev режиме
yarn dev
```

## Установка на сервере

```bash
# Установка пакетов
yarn install

# Сборка приложения
yarn build

# Запуск приложения
yarn preview
```

