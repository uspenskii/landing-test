#!/bin/bash
#
# Запуск сборки промышленного образа

docker build --network host -t lmacmsfe:$1 -t dockerhub.lmsoftware.ru/lmacmsfe:$1 -t dockerhub.lmsoftware.ru/lmacmsfe:latest -f ./Dockerfile.build .

docker login -u repouser -p lmsoftware dockerhub.lmsoftware.ru
docker image push dockerhub.lmsoftware.ru/lmacmsfe:$1
docker image push dockerhub.lmsoftware.ru/lmacmsfe:latest

