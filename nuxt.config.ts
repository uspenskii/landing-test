import { defineNuxtConfig } from "nuxt/config";

export default defineNuxtConfig({
  css: ["~/assets/global.scss"],
  ssr: false,  
  modules: ["@vueuse/nuxt", "nuxt-swiper", "@nuxtjs/i18n", "@nuxt/image"],
  i18n: {
    legacy: false,
    // lazy: true,
    // strategy: "no_prefix",
    strategy: "prefix_except_default",
    defaultLocale: "ru-Ru",
    langDir: "locales",
    detectBrowserLanguage: {
      useCookie: true,
      cookieKey: 'language',
      redirectOn: 'root' // recommended
    },
    locales: [
      {
        code: "ru-Ru",
        iso: "ru-Ru",
        name: "Russian",
        file: "ru.json",
      },
      {
        code: "en-US",
        iso: "en-US",
        name: "English",
        file: "en.json",
      },      
      {
        code: "es-ES",
        iso: "es-ES",
        name: "Spanish",
        file: "es.json",
      },      
      {
        code: "pt-Pt",
        iso: "pt-Pt",
        name: "Portuguese",
        file: "pt.json",
      },      
      {
        code: "fr-Fr",
        iso: "fr-Fr",
        name: "French",
        file: "fr.json",
      },      
    ],
  },
  runtimeConfig: {
    API_URL: process.env.NUXT_PUBLIC_BASE_URL,
    public: {
      API_URL: process.env.NUXT_PUBLIC_BASE_URL,
    },
  },
  image: {
    dir: "assets/img",
  },
  vite: {
    css: {
      preprocessorOptions: {
        scss: {
          additionalData: '@use "@/assets/vars.scss" as *;',
        },
      },
    },
  },
  app: {    
    head: {
      script: [
        {
          src: "https://cdnjs.cloudflare.com/ajax/libs/three.js/r134/three.min.js",
          type: "text/javascript",
        },
        {
          src: "https://cdn.jsdelivr.net/npm/vanta/dist/vanta.net.min.js",
          type: "text/javascript",
        },
      ],
    },
  },  
  router: {
    options: {
      strict: false
    }
  },    
  devServer: {
    port: 3002,
  },
  // generate: {
  //   routes: [
  //     '/articles/kognitivnoe-modelirovanie',      
  //     '/articles/tarify',      
  //     '/articles/funkczionalnoe-naznachenie-i-sfery-ispolzovaniya',      
  //     '/articles/sostav-sapr-regium',      
  //     '/articles/minimalnye-trebovaniya-k-apparatnoj-chasti-sppr-regium',      
  //     '/articles/trebovaniya-k-razvitiyu-msu',      
  //     '/articles/czentr-upravleniya-msu-i-analiticheskij-czentr',      
  //     '/articles/komanda-proekta',      
  //     '/articles/kognitivnaya-model-soczialno-ekonomicheskoj-sistemy',      
  //     '/articles/algoritmy-mashinnogo-obucheniya', 
  //     '/articles/tehnicheskaya-podderzhka', 

  //     '/en-US/articles/kognitivnoe-modelirovanie-en',      
  //     '/en-US/articles/tarify-en',      
  //     '/en-US/articles/funkczionalnoe-naznachenie-i-sfery-ispolzovaniya-en',      
  //     '/en-US/articles/sostav-sapr-regium-en',      
  //     '/en-US/articles/minimalnye-trebovaniya-k-apparatnoj-chasti-sppr-regium-en',      
  //     '/en-US/articles/trebovaniya-k-razvitiyu-msu-en',      
  //     '/en-US/articles/czentr-upravleniya-msu-i-analiticheskij-czentr-en',      
  //     '/en-US/articles/komanda-proekta-en',      
  //     '/en-US/articles/kognitivnaya-model-soczialno-ekonomicheskoj-sistemy-en',      
  //     '/en-US/articles/algoritmy-mashinnogo-obucheniya-en', 
  //     '/en-US/articles/tehnicheskaya-podderzhka-en', 
  //   ]
  // }
});
